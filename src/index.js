import React, { useEffect } from 'react';
import { SafeAreaView } from 'react-native';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { PersistGate } from 'redux-persist/integration/react';

import configStore from './configStore';
const { store, persistor } = configStore();

import { Provider } from 'react-redux';

import App from './App';

console.disableYellowBox = true;

export default () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <NavigationContainer>
            <App globalStore={store} />
          </NavigationContainer>
        </PersistGate>
      </Provider>
    </SafeAreaView>
  );
};
