const config = {
  ESP_MODULE: {
    base_url: 'http://192.168.1.200',
  },
  SERVER: {
    url: 'http://192.168.0.102',
    port: 8080,
  },
  baseUser: {
    name: 'Andrew',
  },
};

export default config;
