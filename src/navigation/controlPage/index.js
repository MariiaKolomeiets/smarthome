import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ControlPage from '~/ui/screens/ControlPage';

const Stack = createStackNavigator();

export default function() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{ header: () => {} }}
        name="ControlPageMain"
        component={ControlPage}
      />
    </Stack.Navigator>
  );
}
