import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AddModule from '~/ui/screens/AddModule';

const Stack = createStackNavigator();

export default function() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{ header: () => {} }}
        name="AddModuleMain"
        component={AddModule}
      />
    </Stack.Navigator>
  );
}
