import { CommonActions } from '@react-navigation/native';

const config = {};

function setNavigation(navigation) {
  if (navigation) {
    config.navigation = navigation;
  }
}

function dispatch(action) {
  if (config.navigation && action) {
    config.navigation.dispatch(action);
  }
}

function navigate(name, params) {
  if (config.navigation && name) {
    const action = CommonActions.navigate({ name, params });
    config.navigation.dispatch(action);
  }
}

function goBack() {
  if (config.navigation) {
    let action = CommonActions.goBack({});
    config.navigation.dispatch(action);
  }
}

export default {
  dispatch,
  goBack,
  navigate,
  setNavigation,
};
