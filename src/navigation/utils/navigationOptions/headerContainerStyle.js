const defaultOptions = {
  backgroundColor: '#fff',
};

export default function getHeaderContainerStyle(options = defaultOptions) {
  const { backgroundColor } = { ...defaultOptions, ...options };

  return {
    headerStyle: {
      height: 58,
      backgroundColor: backgroundColor,
      shadowColor: 'rgba(0, 0, 0, 0.08)',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowRadius: 8,
      shadowOpacity: 1,
    },
  };
}
