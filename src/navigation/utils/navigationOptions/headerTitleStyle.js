const defaultOptions = {
  textColor: 'black',
};

export default function getHeaderTitleStyle(options = defaultOptions) {
  const { textColor } = { ...defaultOptions, ...options };

  return {
    headerTitleAllowFontScaling: false,
    headerTintColor: textColor,
    headerTitleStyle: {
      fontSize: 28,
      letterSpacing: 0,
    },
  };
}
