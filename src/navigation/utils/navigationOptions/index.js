import getHeaderContainerStyle from './headerContainerStyle';
import getHeaderTitleStyle from './headerTitleStyle';

const defaultOptions = {
  containerBackgroundColor: 'rgba(255, 255, 255, 1)',
  isRenderBackButton: true,
  isLightArrow: false,
};

export default function getStackNavigationOptions(options = defaultOptions) {
  const { containerBackgroundColor, titleTextColor } = {
    ...defaultOptions,
    ...options,
  };

  return {
    ...getHeaderContainerStyle({
      backgroundColor: containerBackgroundColor,
    }),
    ...getHeaderTitleStyle({
      textColor: titleTextColor,
    }),
  };
}
