import { Easing, Animated } from 'react-native';

export const TransitionConfiguration = () => {
  return {
    containerStyle: {
      backgroundColor: 'transparent',
    },
    transitionSpec: {
      duration: 750,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true,
    },
    screenInterpolator: sceneProps => {
      const { layout, position, scene } = sceneProps;

      const thisSceneIndex = scene.ColorPicker;
      const width = layout.initWidth;

      const translateX = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
        outputRange: [width, 0, -width],
      });

      const opacity = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
        outputRange: [0, 1, 0],
      });

      return {
        opacity: opacity,
        transform: [
          {
            translateX,
          },
        ],
      };
    },
  };
};
