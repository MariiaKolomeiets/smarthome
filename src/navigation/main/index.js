import React from 'react';
import {
  createStackNavigator,
  TransitionPresets,
} from '@react-navigation/stack';

import Modal from '~/ui/screens/Modal';
import ControlPageStack from '../controlPage';
import AddModuleStack from '../addModule';
import Experiments from '~/ui/screens/Experements';

const Stack = createStackNavigator();

const mainStackOptions = {
  headerMode: 'none',
  mode: 'modal',
  initialRouteName: 'ControlPageStack',
  header: null,
};

const screenOptions = {
  headerMode: 'none',
  transparentCard: true,
  cardStyle: { backgroundColor: 'transparent' },
  cardOverlayEnabled: true,
  ...TransitionPresets.ModalPresentationIOS,
};

export default () => (
  <Stack.Navigator screenOptions={screenOptions} {...mainStackOptions}>
    <Stack.Screen name="ControlPage" component={ControlPageStack} />
    <Stack.Screen name="Modal" component={Modal} />
    <Stack.Screen name="AddModule" component={AddModuleStack} />
    <Stack.Screen name="Experiments" component={Experiments} />
  </Stack.Navigator>
);
