import { connect } from 'react-redux';
import { SOCKET_CONNECTION } from '~/modules/socket/actions';
import React, { Component } from 'react';
import { UIManager } from 'react-native';

import RootNavigation, { NavigationService } from './navigation';

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

class App extends Component {
  componentDidMount() {
    this.props.socketConnect();
    NavigationService.setNavigation();
  }

  componentWillUnmount(): void {
    this.props.socketDisconnect();
  }

  render() {
    return <RootNavigation />;
  }
}

export default connect(
  null,
  {
    socketConnect: SOCKET_CONNECTION.SUBSCRIBE.create,
    socketDisconnect: SOCKET_CONNECTION.UNSUBSCRIBE.create,
  },
)(App);
