import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { persistStore } from 'redux-persist';
import { composeWithDevTools } from 'redux-devtools-extension';

import createReduxWaitForMiddleware from '~/utils/middlewares/reduxWaitForAction';
import { rootReducer, rootSaga } from '~/modules';

const sagaMiddleware = createSagaMiddleware();

const logger = createLogger({
  duration: true,
  collapsed: true,
});

function configMiddleware() {
  let middleware = [];

  middleware.push(sagaMiddleware);
  middleware.push(createReduxWaitForMiddleware());

  if (__DEV__) {
    middleware.push(logger);
  }

  return middleware;
}

const composer = window.__DEV__ // eslint-disable-line no-underscore-dangle
  ? composeWithDevTools
  : compose;

export default () => {
  let store = createStore(
    rootReducer,
    composer(applyMiddleware(...configMiddleware())),
  );

  sagaMiddleware.run(rootSaga);

  let persistor = persistStore(store);
  return { store, persistor };
};
