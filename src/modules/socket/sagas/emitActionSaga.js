import { socketClient } from './socketCreator';

export default function*(action) {
  socketClient.emit(action.id, action.payload);
}
