import { all, takeEvery } from 'redux-saga/effects';
import listenSocketServerSaga from './listenSocketServerSaga';
import registerClientSaga from './registerClientSaga';
import emitActionSaga from './emitActionSaga';
import {
  SOCKET_CONNECTION,
  SOCKET_REGISTER_CLIENT,
  SOCKET_EMMIT_ACTION,
} from '~/modules/socket/actions';

export default function*() {
  yield all([
    takeEvery(SOCKET_CONNECTION.SUBSCRIBE.type, listenSocketServerSaga),
    takeEvery(SOCKET_REGISTER_CLIENT.STATE.type, registerClientSaga),
    takeEvery(SOCKET_EMMIT_ACTION(null).STATE.type, emitActionSaga),
  ]);
}
