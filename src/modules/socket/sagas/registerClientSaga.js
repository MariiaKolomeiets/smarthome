import { socketClient } from './socketCreator';

export default function*(action) {
  socketClient.emit('registerClient', action.payload);
}
