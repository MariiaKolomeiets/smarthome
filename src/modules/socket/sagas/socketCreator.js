import io from 'socket.io-client';
import config from '~/config';

const socketServerURL = 'http://172.16.0.98:8080';

export let socketClient = null;

export const connect = () => {
  const { url, port } = config.SERVER;
  socketClient = io(`${url}:${port}`);
  return new Promise(resolve => {
    socketClient.on('connect', () => {
      resolve(socketClient);
    });
  });
};
