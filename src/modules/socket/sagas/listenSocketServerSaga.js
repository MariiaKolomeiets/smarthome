import { take, put, call, takeEvery } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import { SOCKET_CONNECTION } from '~/modules/socket/actions';
import { connect } from './socketCreator';

const createSocketChannel = socket =>
  eventChannel(emit => {
    const handler = data => {
      emit(data);
    };
    socket.on('registerClient', handler);
    return () => {
      socket.off();
    };
  });

function* handleEvent(event) {
  console.log(event);
}

export default function*() {
  const socket = yield call(connect);
  const socketChannel = yield call(createSocketChannel, socket);
  while (true) {
    yield takeEvery(socketChannel, handleEvent);

    yield take(SOCKET_CONNECTION.UNSUBSCRIBE.type);
    socket.off();
  }
}
