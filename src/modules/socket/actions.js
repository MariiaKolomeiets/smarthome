import { ACTIONS_SUBTYPES, createAction } from '~/modules/utils/actions';

export const SOCKET_CONNECTION = createAction('SOCKET_CONNECTION', {
  [ACTIONS_SUBTYPES.SUBSCRIBE]: true,
  [ACTIONS_SUBTYPES.UNSUBSCRIBE]: true,
});

export const SOCKET_REGISTER_CLIENT = createAction('SOCKET_REGISTER_CLIENT', {
  [ACTIONS_SUBTYPES.STATE]: true,
});

export const SOCKET_EMMIT_ACTION = createAction(
  'SOCKET_EMMIT_ACTION',
  {
    [ACTIONS_SUBTYPES.STATE]: data => data,
  },
  true,
);
