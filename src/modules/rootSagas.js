import { all } from 'redux-saga/effects';
import addModuleSaga from './addModule/sagas';
import deviceInfoSaga from './deviceInfo/sagas';
import socketSaga from './socket/sagas';

import sagaExperiments from './sagaExperiments/sagas';

function* rootSaga() {
  yield all([
    deviceInfoSaga(),
    addModuleSaga(),
    socketSaga(),
    sagaExperiments(),
  ]);
}

export default rootSaga;
