import { ACTIONS_SUBTYPES, createAction } from '~/modules/utils/actions';

export const SET_INTERNET_STATUS = createAction('SET_INTERNET_STATUS', {
  [ACTIONS_SUBTYPES.STATE]: status => status,
});

export const SET_INTERNET_STATUS_SUBSCRIPTION = createAction(
  'SET_INTERNET_SUBSCRIPTION',
  {
    [ACTIONS_SUBTYPES.SUBSCRIBE]: true,
    [ACTIONS_SUBTYPES.UNSUBSCRIBE]: true,
  },
);
