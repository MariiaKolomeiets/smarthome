import { take, put, takeEvery, select } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import NetInfo from '@react-native-community/netinfo';
import {
  SET_INTERNET_STATUS_SUBSCRIPTION,
  SET_INTERNET_STATUS,
} from '~/modules/deviceInfo/actions';

function createInternetStatusChannel() {
  return eventChannel(listener => {
    const handleConnectivityChange = status => {
      listener(status.isConnected);
    };

    const unsubscribe = NetInfo.addEventListener(handleConnectivityChange);
    return () => {
      unsubscribe();
    };
  });
}

export function* subscribeToInternetStatusSaga() {
  const channel = createInternetStatusChannel();
  yield takeEvery(channel, function*(neValue) {
    const {
      deviceInfo: { internetStatus },
    } = yield select();
    if (internetStatus !== neValue) {
      yield put(SET_INTERNET_STATUS.STATE.create(neValue));
    }
  });

  yield take(SET_INTERNET_STATUS_SUBSCRIPTION.UNSUBSCRIBE.type);
  channel.close();
}
