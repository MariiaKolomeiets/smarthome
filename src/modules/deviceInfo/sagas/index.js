import { all, takeEvery } from 'redux-saga/effects';
import { subscribeToInternetStatusSaga } from './internetConnectionSaga';
import { SET_INTERNET_STATUS_SUBSCRIPTION } from '~/modules/deviceInfo/actions';

export default function*() {
  yield all([
    takeEvery(
      SET_INTERNET_STATUS_SUBSCRIPTION.SUBSCRIBE.type,
      subscribeToInternetStatusSaga,
    ),
  ]);
}
