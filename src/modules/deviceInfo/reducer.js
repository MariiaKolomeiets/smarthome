import { SET_INTERNET_STATUS } from './actions';

const initialState = {
  internetStatus: true,
};

export default function(state = initialState, actions) {
  switch (actions.type) {
    case SET_INTERNET_STATUS.STATE.type:
      return {
        ...state,
        internetStatus: actions.payload,
      };
    default:
      return state;
  }
}
