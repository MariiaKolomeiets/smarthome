import { createAction, ACTIONS_SUBTYPES } from '~/modules/utils/actions';

export const WIFI_POINTS_LOAD = createAction('WIFI_POINTS_LOAD', {
  [ACTIONS_SUBTYPES.START]: data => data,
  [ACTIONS_SUBTYPES.SUCCESS]: wifiPoints => wifiPoints,
  [ACTIONS_SUBTYPES.FAILED]: reason => ({
    reason,
  }),
});

export const SET_WIFI_CREDENTIALS = createAction('SET_WIFI_CREDENTIALS', {
  [ACTIONS_SUBTYPES.START]: data => data,
  [ACTIONS_SUBTYPES.SUCCESS]: status => status,
  [ACTIONS_SUBTYPES.FAILED]: reason => ({
    reason,
  }),
});

export const MEMORIZE_WIFI_POINT = createAction('MEMORIZE_WIFI_POINT', {
  [ACTIONS_SUBTYPES.START]: wifiPoint => wifiPoint,
});
