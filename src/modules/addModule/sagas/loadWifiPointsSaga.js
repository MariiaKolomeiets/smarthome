import { put, call } from 'redux-saga/effects';
import { WIFI_POINTS_LOAD } from '~/modules/addModule/actions';

import api from '~/api';

export default function* loadWifiPointsSaga() {
  const { response, error } = yield call(api.addModule.loadWifiPoints);
  if (error) {
    yield put(WIFI_POINTS_LOAD.FAILED.create(error.message));
  } else if (response) {
    yield put(WIFI_POINTS_LOAD.SUCCESS.create(response));
  }
}
