import { put, call } from 'redux-saga/effects';
import { SET_WIFI_CREDENTIALS, MEMORIZE_WIFI_POINT } from '../actions';
import config from '~/config';

import api from '~/api';
import { Alert } from 'react-native';

export default function* setWifiCredentialsSaga(action) {
  const serverCredentials = {
    serverUrl: config.SERVER.url,
    serverPort: config.SERVER.port,
  };
  const { response, error } = yield call(api.addModule.setWifiCredentials, {
    ...action.payload,
    ...serverCredentials,
  });
  if (error) {
    yield put(SET_WIFI_CREDENTIALS.FAILED.create(error.message));
  } else if (response) {
    Alert.alert(
      'Подключение успешно',
      'Сохранить точку доступа?',
      [
        {
          text: 'Отменить',
          style: 'cancel',
        },
        {
          text: 'Сохранить',
          onPress: () => {
            put(MEMORIZE_WIFI_POINT.START.create(action.payload));
          },
        },
      ],
      { cancelable: false },
    );
    yield put(SET_WIFI_CREDENTIALS.SUCCESS.create(action.payload.name));
  }
}
