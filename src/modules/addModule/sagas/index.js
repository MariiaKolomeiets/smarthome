import { all, takeLatest } from 'redux-saga/effects';
import loadWifiPointsSaga from './loadWifiPointsSaga';
import setWifiCredentialsSaga from './setWifiCredentialsSaga';

import { WIFI_POINTS_LOAD, SET_WIFI_CREDENTIALS } from '../actions';

export default function*() {
  yield all([
    takeLatest(WIFI_POINTS_LOAD.START.type, loadWifiPointsSaga),
    takeLatest(SET_WIFI_CREDENTIALS.START.type, setWifiCredentialsSaga),
  ]);
}
