import {
  WIFI_POINTS_LOAD,
  SET_WIFI_CREDENTIALS,
  MEMORIZE_WIFI_POINT,
} from './actions';

const initialState = {
  currentWifiPoint: '',
  wifiPointsList: [],
  memorizedWifiPointsList: [],
};

export default function(state = initialState, actions) {
  switch (actions.type) {
    case WIFI_POINTS_LOAD.SUCCESS.type:
      return {
        ...state,
        wifiPointsList: [...state.wifiPointsList, ...actions.payload],
      };
    case SET_WIFI_CREDENTIALS.SUCCESS.type:
      return {
        ...state,
        currentWifiPoint: actions.payload,
      };
    case MEMORIZE_WIFI_POINT.START.type:
      return {
        ...state,
        memorizedWifiPointsList: [
          actions.payload,
          ...state.memorizedWifiPointsList,
        ],
      };
    default:
      return state;
  }
}
