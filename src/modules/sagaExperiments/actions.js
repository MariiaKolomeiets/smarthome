import { ACTIONS_SUBTYPES, createAction } from '~/modules/utils/actions';

export const SAY_SOMETHING = createAction(
  'SAY_SOMETHING',
  {
    [ACTIONS_SUBTYPES.STATE]: true,
  },
  true,
);
export const SAY_BYE = createAction(
  'SAY_BYE',
  {
    [ACTIONS_SUBTYPES.STATE]: true,
  },
  true,
);
