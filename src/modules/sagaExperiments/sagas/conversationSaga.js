import { take, put, delay, all, takeEvery } from 'redux-saga/effects';
import { SAY_SOMETHING, SAY_BYE } from '../actions';

export function* companionSaga(companionId) {
  let leftConv = false;
  let count = 0;
  while (true) {
    const action = yield take([
      SAY_SOMETHING(null).STATE.type,
      SAY_BYE(null).STATE.type,
    ]);

    switch (action.type) {
      case SAY_SOMETHING(null).STATE.type:
        if (action.id !== companionId) {
          if (count !== 2) {
            count++;
            yield delay(1000);
            yield put(SAY_SOMETHING(companionId).STATE.create());
          } else {
            yield delay(1000);
            yield put(SAY_BYE(companionId).STATE.create());
          }
        }
        break;
      case SAY_BYE(null).STATE.type:
        if (!leftConv && action.id !== companionId) {
          leftConv = true;
          yield delay(1000);
          yield put(SAY_BYE(companionId).STATE.create());
        }
        break;
    }
  }
}

export function* convSaga() {
  yield all([companionSaga(1), companionSaga(2)]);
}
