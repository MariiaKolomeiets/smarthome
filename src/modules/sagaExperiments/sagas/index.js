import { all, takeEvery } from 'redux-saga/effects';
import { companionOneSaga, companionTwoSaga, convSaga } from './conversationSaga';
import { SAY_SOMETHING } from '../actions';

export default function*() {
  yield all([
    convSaga(),
    // takeEvery(SAY_SOMETHING(null).STATE.type, companionOneSaga),
    // takeEvery(SAY_SOMETHING(null).STATE.type, companionTwoSaga),
  ]);
}
