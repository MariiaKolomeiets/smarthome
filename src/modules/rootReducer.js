import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';

import addModuleReducer from './addModule/reducer';
import requestsStatusesReducer from './requestsStatuses/reducer';
import deviceInfoReducer from './deviceInfo/reducer';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  timeout: 120000,
  whitelist: [],
};

export default persistReducer(
  persistConfig,
  combineReducers({
    addModule: addModuleReducer,
    deviceInfo: deviceInfoReducer,
    requestsStatuses: requestsStatusesReducer,
  }),
);
