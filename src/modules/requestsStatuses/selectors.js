import lodashGet from 'lodash/get';

import { ACTIONS_SUBTYPES } from '../utils/actions';

const requestsStatusesRootSelector = state => state.requestsStatuses;

export const getRequestsStatusForActionSelector = (state, action) =>
  lodashGet(
    requestsStatusesRootSelector(state),
    action.id !== undefined
      ? `${action.majorType}.${action.id}`
      : `${action.majorType}`,
  );

export const isLoadingSelector = (state, action) => {
  const requestData = getRequestsStatusForActionSelector(state, action);

  return !!(requestData && requestData.status === ACTIONS_SUBTYPES.START);
};
