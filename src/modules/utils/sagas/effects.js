import { eventChannel, take, put, channel } from 'redux-saga';
import NetInfo from '@react-native-community/netinfo';

function* takeSubscription(pattern, saga, ...args) {
  /*  const channel = eventChannel(listener => {
    const handleConnectivityChange = ({ type }) => {
      listener(type);
    };

    const unsubscribe = NetInfo.addEventListener(handleConnectivityChange);
    return () => unsubscribe();
  });

  while (true) {
    const connectionInfo = yield take(channel);
    yield put({ type: syncActionName, status: connectionInfo }); // blocking action
  }*/
}

function* watchAndLog() {
  while (true) {
    const action = yield take('*');
    const state = yield select();

    console.log('action', action);
    console.log('state after', state);
  }
}
