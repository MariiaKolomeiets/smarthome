import { connect } from 'react-redux';
import React, { useContext, useEffect, useState, useRef } from 'react';
import { Animated, Easing } from 'react-native';

const withModalAnimation = Component => {
  function ComponentWithAnimation(allProps) {
    const { isModalFocused, ...props } = allProps;

    const [isOpen, setIsOpen] = useState(false);

    useEffect(() => {
      console.log(isModalFocused);
      setIsOpen(isModalFocused);
    }, [isModalFocused]);

    const animatedValue = useRef(new Animated.Value(1)).current;

    useEffect(() => {
      Animated.spring(animatedValue, {
        toValue: isOpen ? 0.96 : 1,
        duration: 250,
        easing: Easing.linear,
        useNativeDriver: true,
      }).start();
    }, [animatedValue, isOpen, isModalFocused]);

    const animatedStyle = {
      transform: [{ scale: animatedValue }],
    };
    return (
      <Animated.View
        style={[{ flex: 1, justifyContent: 'flex-end' }, animatedStyle]}
      >
        <Component {...props} />
      </Animated.View>
    );
  }

  Object.assign(ComponentWithAnimation, Component);

  const mapStateToProps = state => ({
    isModalFocused: state.modalPage.isModalFocused,
  });

  return connect(mapStateToProps)(ComponentWithAnimation);
};

export default withModalAnimation;
