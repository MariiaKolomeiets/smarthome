export default [
  {
    value: 0,
    name: 'Конфети',
  },
  {
    value: 1,
    name: 'Огонь',
  },
  {
    value: 2,
    name: 'Радуга вертильно',
  },
  {
    value: 3,
    name: 'Радуга горизонтально',
  },
  {
    value: 4,
    name: 'Смена цвета',
  },
  {
    value: 5,
    name: 'Безумие',
  },
  {
    value: 6,
    name: 'Облака',
  },
  {
    value: 7,
    name: 'Лава',
  },
  {
    value: 8,
    name: 'Плазма',
  },
  {
    value: 9,
    name: 'Радуга',
  },
  {
    value: 10,
    name: 'Павлиин',
  },
  {
    value: 11,
    name: 'Зебра',
  },
  {
    value: 12,
    name: 'Лес',
  },
  {
    value: 13,
    name: 'Океан',
  },
  {
    value: 14,
    name: 'Цвет',
  },
  {
    value: 15,
    name: 'Снег',
  },
  {
    value: 16,
    name: 'Матрица',
  },
  {
    value: 17,
    name: 'Светлячки',
  },
];
