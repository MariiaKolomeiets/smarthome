export const request = (url, method, data, headers) => {
  return new Promise((resolve, reject) => {
    console.log('API REQUEST', url, data);
    fetch(url, {
      method: method || 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        ...headers,
      },
      // mode: 'cors',
      body: (method !== 'GET' && data && JSON.stringify(data)) || undefined,
    })
      .then(response => {
        if (method !== 'DELETE') {
          return response.json();
        }
        return {};
      })
      .then(response => {
        console.log('API RESPONSE', response);
        return response;
      })
      .then(resolve)
      .catch(error => {
        throw error;
      })
      .catch(reject);
  });
};

export const get = (url, filter, headers) => {
  return request(url, 'GET', null, headers);
};

export const post = (url, data, headers) => {
  return request(url, 'POST', data, headers);
};

export const put = (url, data, headers) => {
  return request(url, 'PUT', data, headers);
};

export const del = url => {
  return request(url, 'DELETE');
};

export const patch = (url, data, headers) => {
  return request(url, 'PATCH', data, headers);
};
