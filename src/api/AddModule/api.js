import { get, post } from '~/api/api';
import config from '~/config';

const { ESP_MODULE } = config;

export const getWifiPoints = () =>
  get(`${ESP_MODULE.base_url}/get_wifi_points`);

export const setWifiCredentials = credentials =>
  post(`${ESP_MODULE.base_url}/set_wifi_credentials`, credentials);
