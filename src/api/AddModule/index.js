import loadWifiPoints from './loadWifiPoints';
import setWifiCredentials from './setWifiCredentials';

export default {
  loadWifiPoints,
  setWifiCredentials,
};
