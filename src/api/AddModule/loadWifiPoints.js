import { getWifiPoints } from './api';

export default function() {
  return new Promise(async resolve => {
    const result = {
      response: null,
      error: null,
    };

    try {
      result.response = await getWifiPoints();
    } catch (err) {
      result.error = err;
    }
    resolve(result);
  });
}
