const colors = {
  accent: '#F3534A',
  primary: '#FFCE00',
  secondary: '#2BDA8E',
  tertiary: '#FFE358',
  black: '#323643',
  white: '#FFFFFF',
  gray: '#DDDDDD',
  border: 'rgba(202, 202, 202, 0.7)',
  hiddenBorder: 'rgba(202, 202, 202, 0.1)',
  placeholder: 'rgb(203, 205, 207)',
  error: 'rgb(218, 28, 28)',
  statusBar: {
    light: '#FFFFFF',
    dark: '#000',
  },
};

const sizes = {
  // global sizes
  base: 16,
  font: 14,
  radius: 6,
  padding: 25,

  // font sizes
  h1: 26,
  h2: 20,
  h3: 18,
  title: 18,
  header: 16,
  body: 14,
  caption: 12,
  containerWidth: '94%',
  containerPaddingH: 0,
};

const fonts = {
  h1: {
    fontSize: sizes.h1,
  },
  h2: {
    fontSize: sizes.h2,
  },
  h3: {
    fontSize: sizes.h3,
  },
  header: {
    fontSize: sizes.header,
  },
  title: {
    fontSize: sizes.title,
  },
  body: {
    fontSize: sizes.body,
  },
  caption: {
    fontSize: sizes.caption,
  },
};

const borderParams = {
  borderRadius: 8,
  marginVertical: 8,
  borderWidth: 0.3,
  borderColor: colors.border,
};

export { colors, sizes, fonts, borderParams };
