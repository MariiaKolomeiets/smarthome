import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

const FlexView = ({
  flex,
  align,
  justify,
  dir,
  children,
  styles,
  wrap,
  ...rest
}) => {
  return (
    <View
      {...rest}
      style={[
        flex && { flex: flex },
        align && { alignItems: align },
        justify && { justifyContent: justify },
        dir && { flexDirection: dir },
        wrap && { flexWrap: wrap },
        styles,
      ]}
    >
      {children}
    </View>
  );
};

FlexView.propTypes = {
  flex: PropTypes.number,
  align: PropTypes.oneOf([
    'flex-start',
    'flex-end',
    'center',
    'stretch',
    'baseline',
  ]),
  justify: PropTypes.oneOf([
    'space-between',
    'space-around',
    'center',
    'flex-start',
    'flex-end',
  ]),
  dir: PropTypes.oneOf(['row', 'column', 'row-reverse', 'column-reverse']),
  children: PropTypes.node,
  styles: PropTypes.object,
  wrap: PropTypes.string,
};

FlexView.defaultProps = {
  styles: {},
  flex: 1,
  align: 'center',
  justify: 'center',
};

export default FlexView;
