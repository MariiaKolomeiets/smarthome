/* eslint-disable react/require-default-props,max-len */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TouchableOpacity, Switch } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/FontAwesome';
import Text from './Text';
import { colors } from '../style/theme';

const styles = StyleSheet.create({
  wrapper: {
    width: '100%',
    display: 'flex',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderTopColor: colors.border,
    borderBottomColor: colors.border,
    borderTopWidth: 0.3,
    borderBottomWidth: 0.3,
    paddingHorizontal: 5,
    height: 50,
  },
  side: {
    flexDirection: 'row',
    display: 'flex',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
  mainIconWrapper: {
    width: 35,
    height: 35,
    borderRadius: 8,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
    flexDirection: 'column',
  },
});

export default class ListItem extends Component {
  static propTypes = {
    iconName: PropTypes.string,
    text: PropTypes.string,
    subText: PropTypes.string,
    iconColor: PropTypes.string,
    textColor: PropTypes.string,
    iconBg: PropTypes.string,
    style: PropTypes.object,
    forward: PropTypes.bool,
    hideTopBorder: PropTypes.bool,
    hideBottomBorder: PropTypes.bool,
    switchValue: PropTypes.bool,
    switchItem: PropTypes.bool,
    iconSize: PropTypes.number,
    onPress: PropTypes.any,
    switchValueChange: PropTypes.func,
    rightElements: PropTypes.array,
    leftElements: PropTypes.array,
    rightIconAlign: PropTypes.number,
  };

  static defaultProps = {
    style: {},
    rightElements: [],
    leftElements: [],
    iconName: '',
    text: '',
    textColor: '',
    iconBg: '',
    subText: '',
    iconColor: '',
    iconSize: 0,
    forward: true,
    hideTopBorder: false,
    hideBottomBorder: false,
    switchValue: false,
    switchItem: false,
    rightIconAlign: 0,
    switchValueChange: () => {},
  };

  render() {
    const {
      iconName,
      iconColor,
      iconSize,
      rightElements,
      text,
      textColor,
      subText,
      onPress,
      forward,
      hideTopBorder,
      hideBottomBorder,
      iconBg,
      switchValue,
      switchValueChange,
      switchItem,
      leftElements,
      rightIconAlign,
      style,
    } = this.props;

    const Wrapper = onPress ? TouchableOpacity : View;

    const topBorder = hideTopBorder
      ? { borderTopWidth: 0, color: colors.hiddenBorder }
      : {};
    const bottomBorder = hideBottomBorder
      ? { borderBottomWidth: 0, color: colors.hiddenBorder }
      : {};
    const iconBgColor = iconBg
      ? { backgroundColor: iconBg }
      : { marginRight: 10 };
    const iconColorProp = iconColor || (iconBg && '#fff') || '#898989';

    const wrapperStyles = [styles.wrapper, style, topBorder, bottomBorder];

    return (
      <Wrapper style={wrapperStyles} onPress={onPress || undefined}>
        <View style={[styles.side]}>
          {leftElements}
          {iconName ? (
            <View style={[styles.mainIconWrapper, iconBgColor]}>
              <Icon
                name={iconName}
                size={iconSize || 24}
                style={{
                  color: iconColorProp,
                  marginRight: 0 - rightIconAlign,
                }}
              />
            </View>
          ) : null}
          <Text weight="400" color={textColor}>
            {text}
          </Text>
        </View>
        <View style={styles.side}>
          {subText ? <Text gray>{subText}</Text> : null}
          {forward && !switchItem ? (
            <Ionicons
              name="ios-arrow-forward"
              size={20}
              style={{ color: '#898989', marginLeft: 10 }}
            />
          ) : null}
          {switchItem && (
            <Switch value={switchValue} onValueChange={switchValueChange} />
          )}
          {rightElements}
        </View>
      </Wrapper>
    );
  }
}
