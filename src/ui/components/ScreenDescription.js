import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Dimensions } from 'react-native';
import Text from './Text';
import { sizes } from '~ui/style/theme';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  wrapper: {
    width: '100%',
    paddingVertical: 15,
    flexDirection: 'column',
    alignContent: 'flex-start',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  container: {
    flexDirection: 'column',
    width: sizes.containerWidth,
    justifyContent: 'flex-start',
  },
  titleContainer: {
    marginVertical: 10,
  },
});

export default class ScreenDescription extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    style: PropTypes.object,
  };

  static defaultProps = {
    title: '',
    description: '',
    style: {},
  };

  render() {
    const { title, description, style } = this.props;
    return (
      <View style={[styles.wrapper, style]}>
        <View style={styles.container}>
          {title ? (
            <Text style={styles.titleContainer} h1 bold weight="500">
              {title}
            </Text>
          ) : null}
          {description ? (
            <Text height={22} regular>
              {description}
            </Text>
          ) : null}
        </View>
      </View>
    );
  }
}
