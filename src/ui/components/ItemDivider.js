/* eslint-disable react/require-default-props,max-len */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import Text from './Text';
import { colors } from '../style/theme';

const styles = StyleSheet.create({
  wrapper: {
    width: '100%',
    display: 'flex',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderTopColor: colors.border,
    borderBottomColor: colors.border,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    paddingHorizontal: 5,
    height: 40,
  },
});

export default class ItemDivider extends Component {
  static propTypes = {
    title: PropTypes.string,
    size: PropTypes.number,
  };

  static defaultProps = {
    title: '',
    size: 0,
  };

  render() {
    const { title, size } = this.props;

    return (
      <View style={styles.wrapper}>
        <Text weight="700" size={size || 18}>
          {title}
        </Text>
      </View>
    );
  }
}
