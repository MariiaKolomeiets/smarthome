import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { StyleSheet, TextInput, View, TouchableOpacity } from 'react-native';
import { colors } from '../style/theme';

const styles = StyleSheet.create({
  input: {
    paddingLeft: 10,
    margin: 0,
    flex: 1,
    fontSize: 16,
    height: 45,
    color: '#000',
    fontFamily: 'PingFangHK-Light',
    fontWeight: '300',
  },
  inputWrapper: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    borderWidth: 0.4,
    borderColor: '#dddddd',
    borderRadius: 8,
    alignContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  iconWrapper: {
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 8,
    width: 50,
  },
  rightIcon: {
    borderLeftColor: colors.border,
    borderLeftWidth: 0.4,
  },
});

export default class Input extends Component {
  static propTypes = {
    nativeID: PropTypes.string,
    inputType: PropTypes.string,
    iconName: PropTypes.string,
    rightIconName: PropTypes.string,
    style: PropTypes.object,
    iconPressHandler: PropTypes.func,
    rightIconPressHandler: PropTypes.func,
    handleRef: PropTypes.func,
  };

  static defaultProps = {
    style: {},
    iconPressHandler: () => {},
    rightIconPressHandler: () => {},
    handleRef: () => {},
    nativeID: '',
    inputType: '',
    iconName: '',
    rightIconName: '',
  };

  constructor() {
    super();
    this.text = '';
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.text != nextProps.text) {
      this._textInput &&
        this._textInput.setNativeProps({ text: nextProps.amount }); //重点
    }
  }
  setAmout = text => {
    this.text = text;
  };

  getAmout = () => {
    const { handleChangeText } = this.props;
    if (handleChangeText) {
      handleChangeText(this.text);
    }
  };

  render() {
    const {
      nativeID,
      inputType,
      iconPressHandler,
      rightIconPressHandler,
      iconName,
      rightIconName,
      handleRef,
      style,
      ...props
    } = this.props;

    const inputStyles = [styles.input, style];

    const keyboardType = inputType || 'default';

    const IconWrapper = iconPressHandler ? TouchableOpacity : View;
    const LeftIconWrapper = rightIconPressHandler ? TouchableOpacity : View;

    return (
      <View style={[styles.inputWrapper]}>
        {iconName.length ? (
          <IconWrapper style={styles.iconWrapper} onPress={iconPressHandler}>
            <Icon name={iconName} color={colors.gray} size={20} />
          </IconWrapper>
        ) : null}
        <TextInput
          ref={o => {
            this._textInput = o;
            if (handleRef) {
              handleRef(o);
            }
          }}
          style={inputStyles}
          autoComplete="off"
          autoCapitalize="none"
          autoCorrect={false}
          keyboardType={keyboardType}
          selectionColor="#ffcd00"
          numberOfLines={1}
          underlineColorAndroid={'transparent'}
          // onChangeText={this.setAmout}
          // onEndEditing={this.getAmout}
          {...props}
        />
        {rightIconName.length ? (
          <LeftIconWrapper
            style={styles.iconWrapper}
            onPress={rightIconPressHandler}
          >
            <Icon name={rightIconName} color={colors.gray} size={20} />
          </LeftIconWrapper>
        ) : null}
      </View>
    );
  }
}
