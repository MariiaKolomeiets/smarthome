import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';

const styles = StyleSheet.create({
  wrapper: {
    width: '100%',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    height: 60,
    marginTop: 15,
  },
  button: {
    marginTop: 20,
    height: 50,
    backgroundColor: 'rgb(203, 205, 207)',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    width: 200,
  },
  buttonText: {
    color: '#fff',
    fontFamily: 'Helvetica',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default class Button extends Component {
  static propTypes = {
    onPress: PropTypes.func.isRequired,
    onLongPress: PropTypes.func,
    buttonText: PropTypes.string.isRequired,
    style: PropTypes.object,
    enableButton: PropTypes.bool,
  };

  static defaultProps = {
    style: {},
    onLongPress: () => {},
  };

  render() {
    const {
      buttonText,
      enableButton,
      style,
      onPress,
      onLongPress,
      ...props
    } = this.props;

    const isEnable = typeof enableButton === 'undefined' || enableButton;
    const buttonColor = isEnable && { backgroundColor: 'rgb(255, 205, 0)' };

    return (
      <View style={styles.wrapper}>
        <TouchableOpacity
          activeOpacity={isEnable ? 0.2 : 1}
          style={[styles.button, buttonColor, style]}
          onPress={isEnable ? onPress : undefined}
          onLongPress={isEnable ? onLongPress : undefined}
          {...props}
        >
          <Text style={styles.buttonText}>{buttonText || 'Продолжить'}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
