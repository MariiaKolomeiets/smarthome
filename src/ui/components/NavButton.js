import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';
import { colors } from '../style/theme';

const styles = StyleSheet.create({
  button: {
    width: '100%',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.gray,
  },
});

export default class NavButton extends Component {
  static propTypes = {
    onPress: PropTypes.func,
    buttonText: PropTypes.string,
    style: PropTypes.object,
    isEnabled: PropTypes.bool,
  };

  static defaultProps = {
    style: {},
    isEnabled: false,
    buttonText: '',
    onPress: () => {},
  };

  render() {
    const { buttonText, isEnabled, style, onPress, ...props } = this.props;

    const buttonColor = isEnabled && { color: colors.primary };

    return (
      <TouchableOpacity
        activeOpacity={isEnabled ? 0.2 : 1}
        style={[styles.button, style]}
        onPress={isEnabled ? onPress : undefined}
        {...props}
      >
        <Text style={[styles.buttonText, buttonColor]}>
          {buttonText || 'Готово'}
        </Text>
      </TouchableOpacity>
    );
  }
}
