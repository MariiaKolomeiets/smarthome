import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import { sizes } from '../style/theme';

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#fff',
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  container: {
    height: '100%',
    flexDirection: 'column',
    width: sizes.containerWidth,
    justifyContent: 'flex-start',
    alignContent: 'center',
    alignItems: 'center',
  },
  round: {
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
  },
});

export default class Container extends Component {
  static propTypes = {
    children: PropTypes.array.isRequired,
    style: PropTypes.object,
    round: PropTypes.bool,
  };

  render() {
    const { children, style, round } = this.props;

    const roundBlock = round && styles.round;

    return (
      <View style={[styles.wrapper, style, roundBlock]}>
        <View style={styles.container}>{children}</View>
      </View>
    );
  }
}
