/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import { colors } from '../style/theme';

const styles = StyleSheet.create({
  // default style
  text: {
    fontSize: 17,
    color: '#000',
    // fontFamily: 'SFProDisplay-Regular',
    fontWeight: '300',
  },
  // variations
  regular: {
    fontWeight: 'normal',
  },
  bold: {
    // fontFamily: 'SFProDisplay-Bold'
    // fontWeight: 'bold',
  },
  semibold: {
    fontWeight: '500',
  },
  medium: {
    fontWeight: '500',
  },
  light: {
    fontWeight: '200',
  },
  // position
  center: { textAlign: 'center' },
  right: { textAlign: 'right' },
  // colors
  yellow: {
    color: colors.primary,
  },
  primary: {
    color: colors.primary,
  },
  // fonts
  h1: {
    fontSize: 27,
  },
  h2: {
    fontSize: 25,
  },
  h3: {
    fontSize: 22,
  },
  white: {
    color: '#fff',
  },
  gray: {
    color: '#898989',
  },
  paragraph: {
    marginBottom: 15,
  },
});

export default class Typography extends Component {
  render() {
    const {
      h1,
      h2,
      h3,
      title,
      body,
      caption,
      small,
      size,
      transform,
      align,
      // styling
      regular,
      bold,
      semibold,
      medium,
      weight,
      light,
      center,
      right,
      spacing, // letter-spacing
      height, // line-height
      // colors
      color,
      accent,
      primary,
      secondary,
      tertiary,
      black,
      white,
      gray,
      gray2,
      style,
      children,
      yellow,
      paragraph,
      ...props
    } = this.props;

    const textStyles = [
      styles.text,
      h1 && styles.h1,
      h2 && styles.h2,
      h3 && styles.h3,
      title && styles.title,
      body && styles.body,
      caption && styles.caption,
      small && styles.small,
      size && { fontSize: size },
      transform && { textTransform: transform },
      align && { textAlign: align },
      height && { lineHeight: height },
      spacing && { letterSpacing: spacing },
      weight && { fontWeight: weight },
      regular && styles.regular,
      bold && styles.bold,
      semibold && styles.semibold,
      medium && styles.medium,
      light && styles.light,
      center && styles.center,
      right && styles.right,
      color && styles[color],
      color && !styles[color] && { color },
      // color shortcuts
      accent && styles.accent,
      primary && styles.primary,
      secondary && styles.secondary,
      tertiary && styles.tertiary,
      black && styles.black,
      white && styles.white,
      yellow && styles.yellow,
      gray && styles.gray,
      gray2 && styles.gray2,
      paragraph && styles.paragraph,
      style, // rewrite predefined styles
    ];

    if (!children) {
      return null;
    }

    return (
      <Text style={textStyles} {...props}>
        {children}
      </Text>
    );
  }
}
