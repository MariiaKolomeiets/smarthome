import React from 'react';
import { StyleSheet } from 'react-native';
import { FlexView, Text, Button } from '~/ui/components';

export default function({ startConversation }) {
  return (
    <FlexView styles={styles.container}>
      <Button buttonText="Запуск" onPress={startConversation} />
    </FlexView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
  },
});
