import { connect } from 'react-redux';

import { SAY_SOMETHING } from '~/modules/sagaExperiments/actions';
import Experiments from './Experiments';

function mapStateToProps(state) {
  return {
    memorizedPoints: state.addModule.memorizedWifiPointsList,
    wifiPoints: state.addModule.wifiPointsList,
  };
}

const mapDispatchToProps = {
  startConversation: SAY_SOMETHING(1).STATE.create,
};

export default connect(
  null,
  mapDispatchToProps,
)(Experiments);
