import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { borderParams } from '~/ui/style/theme';

import { FlexView, Text } from '~/ui/components';

export default function({ data, onValueChange, currentLightMode }) {
  return (
    <ScrollView style={{ flex: 1 }}>
      <FlexView
        justify="center"
        flex={1}
        align="center"
        styles={styles.container}
      >
        {data.map(({ name, value }) => {
          return (
            <TouchableOpacity
                onPress={() => onValueChange(value)}
                style={styles.modeContainer}
                key={`LightMode-item${value}`}>
              <FlexView>
                <Text align="center" >{name}</Text>
              </FlexView>
            </TouchableOpacity>
          );
        })}
      </FlexView>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 130,
  },
  active: {
    borderWidth: 1.8,
    borderColor: 'black',
  },
  modeContainer: {
    width: '100%',
    height: 60,
    ...borderParams,
  },
  activeMode: {
    borderColor: '#000',
  },
});
