import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgb(241, 242, 247)',
    borderRadius: 12,
    overflow: 'hidden',
    width: 120,
  },
  pendingTrack: {},
  track: {
    flex: 1,
    backgroundColor: 'rgba(255,197,128,0.7)',
    borderRadius: 12,
    alignSelf: 'stretch',
  },
  trackLabel: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  trackLabelText: {
    color: 'white',
    fontWeight: '600',
  },
  thumb: {
    backgroundColor: 'rgba(0,0,0,.5)',
    borderRadius: 12,
    height: 3,
    width: 24,
    marginTop: 6,
    alignSelf: 'center',
  },
});
