import React, { Component } from 'react';
import { Animated, PanResponder, View } from 'react-native';

import styles from './styles';

export default class extends Component {
  static defaultProps = {
    value: 40,
    maximumValue: 100,
    minimumValue: 0,
    onSlidingStart: () => {},
    onValueChange: () => {},
    onSlidingComplete: () => {},
    trackStyle: ''
  };

  constructor(props) {
    super();
    this.state = {
      value: props.value,
    };

    this.range = props.maximumValue - props.minimumValue;
  }

  componentWillMount() {
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (e, gestureState) => true,
      onPanResponderGrant: (evt, gestureState) => {
        this.props.onSlidingStart();
        this.setState({ anchorValue: this.state.value });
      },
      onPanResponderMove: Animated.event([null, {}], {
        listener: this.handleSlide,
      }),
      onPanResponderRelease: (evt, gestureState) => {
        this.props.onSlidingComplete();
      },
    });
  }

  slideTo = value => {
    this.setState({ value });
  };

  onLayout = ({ nativeEvent }) => {
    this.setState({
      width: nativeEvent.layout.width,
      height: nativeEvent.layout.height,
    });
  };

  handleSlide = (evt, gestureState) => {
    const { maximumValue, minimumValue } = this.props;
    let valueIncrement = (-gestureState.dy * this.range) / this.state.height;
    let nextValue = this.state.anchorValue + valueIncrement;
    nextValue = nextValue >= maximumValue ? maximumValue : nextValue;
    nextValue = nextValue <= minimumValue ? minimumValue : nextValue;

    this.props.onValueChange(nextValue);
    this.setState({ value: nextValue });
  };

  render() {
    const value = this.state.value;
    const unitValue = (value - this.props.minimumValue) / this.range;

    return (
      <View
        onLayout={this.onLayout}
        style={[styles.container, this.props.style]}
        {...this.panResponder.panHandlers}
      >
        <View style={[styles.pendingTrack, { flex: 1 - unitValue }]} />
        <View
          style={[styles.track, { flex: unitValue }, this.props.trackStyle]}
        />
      </View>
    );
  }
}

function formatNumber(x) {
  return x.toFixed(1).replace(/\.?0*$/, '');
}
