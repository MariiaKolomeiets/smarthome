import React, { Component, useEffect, useState } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';

import { FlexView } from '~ui/components';

const styles = StyleSheet.create({
  circle: {
    height: 40,
    width: 40,
    borderRadius: 20,
    marginHorizontal: 20,
  },
  container: {
    width: '100%',
    marginTop: 40,
  },
  active: {
    borderWidth: 1.8,
    borderColor: 'black',
  },
});

const defaultColors = ['red', 'green', 'blue'];

export default function(props) {
  const colors = props.colors || defaultColors;
  const activeColor = props.activeColor || colors[0];
  const { handleChange } = props;

  return (
    <FlexView
      justify="center"
      flex={1}
      align="center"
      dir="row"
      styles={styles.container}
    >
      {colors.map(color => {
        const style = [styles.circle, { backgroundColor: color }];
        if (activeColor === color) {
          style.push(styles.active);
        }
        return (
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => handleChange(color)}
            style={style}
          />
        );
      })}
    </FlexView>
  );
}
