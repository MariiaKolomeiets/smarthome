import React from 'react';
import { ScrollView } from 'react-native';

import { Container, ScreenDescription } from '../../components';
import { Light, CoffeeMachine } from './screens';

export default function(props) {
  return (
    <Container>
      <ScreenDescription title="Настройка освещения" description="Лампа" />
      <CoffeeMachine {...props} />
    </Container>
  );
}
