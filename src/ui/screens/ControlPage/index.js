import { connect } from 'react-redux';
import ControlPage from './ControlPage';

function mapStateToProps(state) {
  return {};
}

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ControlPage);
