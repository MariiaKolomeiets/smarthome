import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import { Dimmer, ColorPalette, LightModes } from '../../components';
import { FlexView, Text, Button } from '~/ui/components';
import { lightModes } from '~/utils/constans';
import { borderParams } from '~/ui/style/theme';

export default function(props) {
  const colorState = 14;

  const [currentLightMode, setLightMode] = useState(0);
  const deviceId = 1;

  function navigateToColorPicker() {
    props.navigation.navigate('Modal', {
      screen: 'Modal',
      screenType: 'colorPicker',
      modalMode: 'light',
      deviceId,
    });
  }

  const changeBrightness = value => props.changeBrightness({ value, deviceId });
  const changeLightMode = value => {
    setLightMode(value);
    props.changeLightMode({ value, deviceId });
  };
  const changeLightSpeed = value => props.changeLightSpeed({ value, deviceId });

  return (
    <FlexView justify="flex-start" align="center">
      <FlexView justify="space-between" dir="row" flex={3}>
        <FlexView flex={1}>
          <LightModes
            currentLightMode={currentLightMode}
            onValueChange={changeLightMode}
            data={lightModes}
          />
        </FlexView>
        <FlexView dir="row" justify="flex-end" flex={1.6}>
          <FlexView>
            <Icon
              name="ios-flash"
              backgroundColor="#3b5998"
              size={25}
              color={'rgba(0,0,0,0.37)'}
            />
            <Dimmer
              style={styles.dimmer}
              maximumValue={255}
              onValueChange={changeLightSpeed}
              // value={brightness}
            />
          </FlexView>
          <FlexView>
            <Icon
              name="ios-sunny"
              backgroundColor="#3b5998"
              size={25}
              color={'rgba(0,0,0,0.37)'}
            />
            <Dimmer
              style={styles.dimmer}
              maximumValue={255}
              onValueChange={changeBrightness}
              // value={brightness}
            />
          </FlexView>
          <FlexView>
            <Icon
              name="ios-sunny"
              backgroundColor="#3b5998"
              size={25}
              color={'rgba(0,0,0,0.37)'}
            />
            <Dimmer
              style={styles.dimmer}
              maximumValue={255}
              onValueChange={changeBrightness}
              // value={brightness}
            />
          </FlexView>
        </FlexView>
      </FlexView>
      <FlexView justify="flex-start">
        <TouchableOpacity
          onPress={
            colorState === currentLightMode ? navigateToColorPicker : undefined
          }
          activeOpacity={colorState === currentLightMode ? 0 : 1}
          style={styles.currentModeWrapper}
        >
          <Text>{lightModes[currentLightMode].name || ''}</Text>
        </TouchableOpacity>
      </FlexView>
      {/* <ColorPalette handleChange={setActiveColor} activeColor={activeColor} />*/}
    </FlexView>
  );
}

const styles = StyleSheet.create({
  dimmer: {
    width: '90%',
    height: '100%',
  },
  currentModeWrapper: {
    width: 200,
    height: 60,
    ...borderParams,
    marginTop: 30,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
});
