import { connect } from 'react-redux';
import Light from './Light';
import { SOCKET_EMMIT_ACTION } from '~/modules/socket/actions';

function mapStateToProps(state) {
  return {};
}

const mapDispatchToProps = {
  changeBrightness: SOCKET_EMMIT_ACTION('brightness').STATE.create,
  changeLightSpeed: SOCKET_EMMIT_ACTION('lightSpeed').STATE.create,
  changeLightMode: SOCKET_EMMIT_ACTION('lightMode').STATE.create,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Light);
