import Light from './Light';
import CoffeeMachine from './CoffeeMachine';

export { Light, CoffeeMachine };
