import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { FlexView, Text, Button, ListItem } from '~/ui/components';
import { borderParams } from '~/ui/style/theme';

export default function({ setStart, setStop }) {
  return (
    <View style={{ width: '100%' }}>
      <ListItem
        text="Время включения"
        rightElements={() => [<Text>Hello</Text>]}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  dimmer: {
    width: '90%',
    height: '100%',
  },
  currentModeWrapper: {
    width: 200,
    height: 60,
    ...borderParams,
    marginTop: 30,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
});
