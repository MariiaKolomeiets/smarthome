import { connect } from 'react-redux';
import CoffeeMachine from './CoffeeMachine';
import { SOCKET_EMMIT_ACTION } from '~/modules/socket/actions';

function mapStateToProps(state) {
  return {};
}

const mapDispatchToProps = {
  setStart: SOCKET_EMMIT_ACTION('change_start').STATE.create,
  setStop: SOCKET_EMMIT_ACTION('change_stop').STATE.create,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CoffeeMachine);
