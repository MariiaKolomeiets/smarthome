import { connect } from 'react-redux';

import { MEMORIZE_WIFI_POINT } from '~/modules/addModule/actions';
import Modal from './Modal';

import { isLoadingSelector } from '~/modules/requestsStatuses/selectors';

function mapStateToProps(state) {
  return {
    isLoading: isLoadingSelector(state, MEMORIZE_WIFI_POINT),
  };
}

export default connect(mapStateToProps)(Modal);
