import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import Spinner from 'react-native-spinkit';

import { Text } from '~/ui/components';
import { colors } from '~/ui/style/theme';

const styles = StyleSheet.create({
  wrapper: {
    width: '100%',
    borderBottomWidth: 0.4,
    borderBottomColor: colors.border,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  block: {
    flex: 1,
  },
  container: {
    width: '90%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
    paddingVertical: 20,
  },
});

export default function({
  title,
  close,
  action,
  activeButton,
  isLoading,
  okButton,
}) {
  const ActionWrapper = activeButton ? TouchableOpacity : View;
  return (
    <View style={styles.wrapper}>
      <View style={styles.container}>
        <TouchableOpacity onPress={close} style={styles.block}>
          <Text primary>Отмена</Text>
        </TouchableOpacity>
        <Text align={'center'}>{title}</Text>
        {isLoading ? (
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
            }}
          >
            <Spinner
              // style={styles.spinner}
              isVisible={true}
              size={18}
              type={'Circle'}
              color={colors.primary}
            />
          </View>
        ) : (
          <ActionWrapper
            onPress={activeButton ? action : undefined}
            style={[styles.block, { alignItems: 'flex-end' }]}
          >
            <Text primary gray={!activeButton}>
              {okButton || 'Подкл.'}
            </Text>
          </ActionWrapper>
        )}
      </View>
    </View>
  );
}
