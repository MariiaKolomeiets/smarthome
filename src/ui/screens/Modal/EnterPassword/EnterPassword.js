import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { Text, Input } from '~/ui/components';
import { Header } from '../components/index';

const styles = StyleSheet.create({
  wrapper: {
    width: '100%',
    flex: 1,
  },
});

export default function(props) {
  const { navigation, route, isLoading } = props;

  const { title, point } = route;
  const screenTitle = title || '';
  const { name, locked, password } = point || {};

  const [passwordInput, setPassword] = useState(password || '');
  const [visiblePass, toggleVisiblePass] = useState(false);
  const [isValidPassword, handleValidPassword] = useState(false);

  useEffect(() => handleValidPassword(validatePassword(passwordInput)), [
    passwordInput,
    validatePassword,
  ]);

  async function connectToPoint() {
    props.setWifiCredentials({
      name,
      password: passwordInput,
    });
  }

  function validatePassword(password) {
    return !locked || password.length > 4;
  }

  return (
    <>
      <Header
        title={screenTitle}
        close={navigation.goBack}
        action={connectToPoint}
        activeButton={isValidPassword}
        isLoading={isLoading}
      />
      <View style={styles.wrapper}>
        <Input
          placeholder={'Введите пароль'}
          onChangeText={setPassword}
          value={passwordInput}
          secureTextEntry={!visiblePass}
          rightIconName={visiblePass ? 'eye-slash' : 'eye'}
          rightIconPressHandler={() => toggleVisiblePass(!visiblePass)}
        />
      </View>
    </>
  );
}
