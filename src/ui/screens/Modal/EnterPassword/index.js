import { connect } from 'react-redux';
import EnterPassword from './EnterPassword';

import { SET_WIFI_CREDENTIALS } from '~/modules/addModule/actions';

import { isLoadingSelector } from '~/modules/requestsStatuses/selectors';

function mapStateToProps(state) {
  return {
    isLoading: isLoadingSelector(state, SET_WIFI_CREDENTIALS),
  };
}

const mapDispatchToProps = {
  setWifiCredentials: SET_WIFI_CREDENTIALS.START.create,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EnterPassword);
