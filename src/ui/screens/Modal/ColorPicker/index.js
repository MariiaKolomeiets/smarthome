import { connect } from 'react-redux';
import ColorPicker from './ColorPicker';

import { SOCKET_EMMIT_ACTION } from '~/modules/socket/actions';

const mapStateToProps = (state, props) => {
  const { deviceId } = props.route.params;
  return { deviceId };
};

const mapDispatchToProps = {
  changeLightColor: SOCKET_EMMIT_ACTION('lightColor').STATE.create,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ColorPicker);
