import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import { Header } from '../components/index';

import {
  SlidersColorPicker,
  HueSlider,
  SaturationSlider,
  LightnessSlider,
} from 'react-native-color';
import tinycolor from 'tinycolor2';

const modes = {
  hex: {
    getString: color => tinycolor(color).toHexString(),
    label: 'HEX',
  },
  hsl: {
    getString: color => tinycolor(color).toHslString(),
    label: 'HSL',
  },
  hsv: {
    getString: color => tinycolor(color).toHsvString(),
    label: 'HSV',
  },
  rgb: {
    getString: color => tinycolor(color).toRgbString(),
    label: 'RGB',
  },
};

const swatches = ['#247ba0', '#70c1b3', '#b2dbbf', '#f3ffbd', '#ff1654'];

export default class extends Component {
  constructor(props) {
    super(props);
    const color = tinycolor('#70c1b3').toHsl();
    const mode = tinycolor(this.props.mode).toHsl();
    this.state = {
      color,
      mode: 'hex',
    };
  }

  updateHue = h =>
    this.setState({ color: { ...this.state.color, h } }, this.onChange);
  updateSaturation = s =>
    this.setState({ color: { ...this.state.color, s } }, this.onChange);
  updateLightness = l =>
    this.setState({ color: { ...this.state.color, l } }, this.onChange);

  onChange = () => {
    const { changeLightColor, deviceId } = this.props;
    const { mode, color } = this.state;
    const targetColor = modes[mode].getString(color);
    changeLightColor({ value: targetColor, deviceId });
  };

  closeModal = () => this.props.navigation.goBack();

  render() {
    const colorHex = tinycolor(this.state.color).toHexString();
    return (
      <>
        <Header
          title="Выбор цвета"
          close={this.closeModal}
          activeButton
          okButton="OK"
          action={this.closeModal}
        />
        <View style={styles.container}>
          <View style={styles.content}>
            <View
              style={[
                styles.colorPreview,
                {
                  backgroundColor: colorHex,
                },
              ]}
            />
            <View style={styles.colorString}>
              <Text style={styles.colorStringText}>
                {modes[this.state.mode].getString(this.state.color)}
              </Text>
            </View>
            <View style={styles.modesRow}>
              {Object.keys(modes).map(key => (
                <TouchableOpacity
                  onPress={() => this.setState({ mode: key })}
                  key={key}
                  style={[
                    styles.mode,
                    this.state.mode === key && styles.modeActive,
                  ]}
                >
                  <Text
                    style={[
                      styles.modeText,
                      this.state.mode === key && styles.modeTextActive,
                    ]}
                  >
                    {modes[key].label}
                  </Text>
                </TouchableOpacity>
              ))}
            </View>
            <View style={styles.sliders}>
              <HueSlider
                style={styles.sliderRow}
                gradientSteps={40}
                value={this.state.color.h}
                onValueChange={this.updateHue}
              />
              <SaturationSlider
                style={styles.sliderRow}
                gradientSteps={20}
                value={this.state.color.s}
                color={this.state.color}
                onValueChange={this.updateSaturation}
              />
              <LightnessSlider
                style={styles.sliderRow}
                gradientSteps={20}
                value={this.state.color.l}
                color={this.state.color}
                onValueChange={this.updateLightness}
              />
            </View>
            <View style={styles.swatchesContainer}>
              {swatches.map((swatch, index) => (
                <TouchableOpacity
                  key={swatch}
                  style={[
                    styles.swatch,
                    {
                      backgroundColor: swatch,
                      marginRight: index < swatches.length - 1 ? 16 : 0,
                    },
                  ]}
                  onPress={() =>
                    this.setState({ color: tinycolor(swatch).toHsl() })
                  }
                />
              ))}
            </View>
          </View>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'center',
    marginTop: Platform.OS === 'ios' ? 20 : 0,
    width: '100%',
  },
  header: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 64,
    marginHorizontal: 16,
  },
  headerButton: {
    lineHeight: 22,
    fontSize: 17,
    ...Platform.select({
      android: {
        fontFamily: 'sans-serif-medium',
      },
      ios: {
        fontWeight: '600',
        letterSpacing: -0.41,
      },
    }),
  },
  content: {
    flex: 1,
    marginHorizontal: 16,
  },
  lightText: {
    lineHeight: 22,
    fontSize: 17,
    color: 'white',
    ...Platform.select({
      android: {
        fontFamily: 'sans-serif-medium',
      },
      ios: {
        fontWeight: '600',
        letterSpacing: -0.41,
      },
    }),
  },
  darkText: {
    lineHeight: 22,
    fontSize: 17,
    marginTop: 6,
    ...Platform.select({
      android: {
        fontFamily: 'sans-serif-medium',
      },
      ios: {
        fontWeight: '600',
        letterSpacing: -0.41,
      },
    }),
  },
  colorPreview: {
    height: 90,
    borderRadius: 3,
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    paddingVertical: 8,
    paddingHorizontal: 12,
  },
  modesRow: {
    marginTop: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  mode: {
    paddingHorizontal: 6,
    paddingVertical: 1,
    marginRight: 16,
  },
  modeActive: {
    backgroundColor: 'black',
    borderRadius: 3,
  },
  modeText: {
    lineHeight: 18,
    fontSize: 13,
    ...Platform.select({
      android: {
        fontFamily: 'sans-serif',
      },
      ios: {
        fontWeight: '400',
        letterSpacing: -0.08,
      },
    }),
  },
  modeTextActive: {
    color: 'white',
  },
  sliders: {
    marginTop: 16,
  },
  sliderRow: {
    marginTop: 16,
  },
  colorString: {
    marginTop: 32,
    borderBottomWidth: 2,
    borderColor: '#DDDDDD',
  },
  colorStringText: {
    lineHeight: 24,
    fontSize: 20,
    ...Platform.select({
      android: {
        fontFamily: 'monospace',
      },
      ios: {
        fontFamily: 'Courier New',
        fontWeight: '600',
        letterSpacing: 0.75,
      },
    }),
  },
  swatchesText: {
    marginTop: 16,
    lineHeight: 18,
    fontSize: 13,
    ...Platform.select({
      android: {
        fontFamily: 'sans-serif',
      },
      ios: {
        fontWeight: '400',
        letterSpacing: -0.08,
      },
    }),
    color: '#555',
  },
  swatchesContainer: {
    marginTop: 30,
    marginBottom: 24,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  swatch: {
    flex: 1,
    aspectRatio: 1,
    maxHeight: 100,
    maxWidth: 100,
    borderRadius: 3,
  },
});

SlidersColorPicker.propTypes = {
  visible: PropTypes.bool.isRequired,
  swatches: PropTypes.arrayOf(PropTypes.string).isRequired,
  swatchesLabel: PropTypes.string.isRequired,
  onOk: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  okLabel: PropTypes.string.isRequired,
  cancelLabel: PropTypes.string.isRequired,
  value: PropTypes.string,
};

SlidersColorPicker.defaultProps = {
  okLabel: 'Ok',
  cancelLabel: 'Cancel',
  value: '#70c1b3',
};
