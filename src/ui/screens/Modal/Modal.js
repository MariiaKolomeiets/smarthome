import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Container, FullScreenLoader } from '~/ui/components';

import EnterPassword from './EnterPassword/';
import ColorPicker from './ColorPicker';

const modalScreens = {
  enterPassword: EnterPassword,
  colorPicker: ColorPicker,
};

export default function(props) {
  const {
    route: {
      params: { screenType, modalMode },
    },
    isLoading,
  } = props;
  const Component = modalScreens[screenType];

  return (
    <>
      <Container style={[styles.container, styles[modalMode]]} round>
        <Component {...props} />
      </Container>
      <FullScreenLoader visible={isLoading} />
    </>
  );
}

const styles = StyleSheet.create({
  light: {
    marginTop: '55%',
  },
});
