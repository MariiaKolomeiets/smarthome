import React from 'react';
import { View } from 'react-native';

import { ListItem, ItemDivider } from '../../../components';

export default function({ data, handleWifiPoint, dividerTitle }) {
  if (!data.length) {
    return null;
  }
  return (
    <View style={{ marginVertical: 20 }}>
      <ItemDivider title={dividerTitle} />
      {data.map((point, index) => {
        return (
          <ListItem
            text={point.name}
            key={`${index}-wifi-point`}
            onPress={() => handleWifiPoint(point)}
          />
        );
      })}
    </View>
  );
}
