import React, { useEffect } from 'react';
import { ScrollView } from 'react-native';

import {
  Container,
  ScreenDescription,
  FullScreenLoader,
} from '~/ui/components';
import { WifiList } from './components';

export default function({
  memorizedPoints,
  wifiPoints,
  navigation,
  loadWifiPoints,
  isLoading,
}) {
  useEffect(() => {
    loadWifiPoints();
  }, [loadWifiPoints]);

  function handleWifiPoint(point) {
    navigation.navigate('Modal', {
      screen: 'Modal',
      screenType: 'enterPassword',
      title: 'Введите пароль',
      point,
      key: `wifi-point-key~${point.name}`,
      modalMode: 'light',
    });
  }

  return (
    <Container>
      <ScrollView style={{ width: '100%' }}>
        <ScreenDescription
          title="Доступные точки доступа"
          description="Выберете свою точку доступа для дальнейшего управления модулем"
        />
        <WifiList
          dividerTitle="Использовавшиеся ранее"
          data={memorizedPoints}
          handleWifiPoint={handleWifiPoint}
        />
        <WifiList
          dividerTitle="Доступные сети"
          data={wifiPoints}
          handleWifiPoint={handleWifiPoint}
        />
      </ScrollView>
      <FullScreenLoader visible={isLoading} />
    </Container>
  );
}
