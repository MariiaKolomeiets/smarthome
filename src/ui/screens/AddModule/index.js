import { connect } from 'react-redux';

import { WIFI_POINTS_LOAD } from '~/modules/addModule/actions';
import AddModule from './AddModule';

import { isLoadingSelector } from '~/modules/requestsStatuses/selectors';

function mapStateToProps(state) {
  return {
    isLoading: isLoadingSelector(state, WIFI_POINTS_LOAD),
    memorizedPoints: state.addModule.memorizedWifiPointsList,
    wifiPoints: state.addModule.wifiPointsList,
  };
}

const mapDispatchToProps = {
  loadWifiPoints: WIFI_POINTS_LOAD.START.create,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddModule);
